<?php
namespace Virtualfs;

use Mockery;
use Virtualfs\Test\UnitTestCase;

class FileSystemTest extends UnitTestCase
{
    public function setUp()
    {
        $this->scheme = 'foo';
        $this->wrapperClass = 'Virtualfs\Stream\StreamWrapper';
        $this->logger = Mockery::mock('Psr\Log\LoggerInterface');
        $this->walker = Mockery::mock('Virtualfs\Node\Walker\NodeWalkerInterface');
        $this->factory = Mockery::mock('Virtualfs\Node\Factory\NodeFactoryInterface');
        $this->registry = Mockery::mock('Virtualfs\RegistryInterface');
        $this->root = Mockery::mock('Virtualfs\Node\NodeContainerInterface');

        $this->factory->shouldReceive('buildDirectory')->once()->withNoArgs()->andReturn($this->root);

        $this->fs = new FileSystem($this->scheme, $this->wrapperClass, $this->factory, $this->walker, $this->registry, $this->logger);
    }

    public function testInterface()
    {
        $this->assertInstanceOf('Virtualfs\FileSystemInterface', $this->fs);
    }

    public function testGet()
    {
        $path = '/foo/bar/baz.txt';
        $node = Mockery::mock('Virtualfs\Node\NodeInterface');

        $this->walker->shouldReceive('findNode')->once()->with($this->root, $path)->andReturn($node);

        $this->assertSame($node, $this->fs->get($path));
    }

    public function testGetLogger()
    {
        $this->assertSame($this->logger, $this->fs->getLogger());
    }

    public function testGetNodeFactory()
    {
        $this->assertSame($this->factory, $this->fs->getNodeFactory());
    }

    public function testGetNodeWalker()
    {
        $this->assertSame($this->walker, $this->fs->getNodeWalker());
    }

    public function testMountThrowsRegisteredException()
    {
        $this->registry->shouldReceive('has')->once()->with($this->scheme)->andReturn(true);

        $this->setExpectedException('Virtualfs\Exception\RegisteredSchemeException');

        $this->fs->mount();
    }

    public function testUnmountThrowsUnregisteredException()
    {
        $this->registry->shouldReceive('has')->once()->with($this->scheme)->andReturn(false);

        $this->setExpectedException('Virtualfs\Exception\UnregisteredSchemeException');

        $this->fs->unmount();
    }

    public function testGetScheme()
    {
        $this->assertSame($this->scheme, $this->fs->getScheme());
    }

    public function testGetSchemeWithoutColonSlashSlash()
    {
        $factory = Mockery::mock('Virtualfs\Node\Factory\NodeFactoryInterface');
        $factory->shouldReceive('buildDirectory')->once()->withNoArgs()->andReturn($this->root);

        $fs = new FileSystem("foo://", $this->wrapperClass, $factory, $this->walker, $this->registry, $this->logger);

        $this->assertSame($this->scheme, $fs->getScheme());
    }
}
