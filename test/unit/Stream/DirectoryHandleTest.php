<?php
namespace Virtualfs\Stream;

use Mockery;
use Virtualfs\Test\UnitTestCase;

class DirectoryHandleTest extends UnitTestCase
{
    public function setUp()
    {
        $this->fs = Mockery::mock('VirtualfsFileSystem');
    }

    public function testInterface()
    {
        $handle = new DirectoryHandle($this->fs, '');

        $this->assertInstanceOf('VirtualfsStream\HandleInterface', $handle);
    }

    public function testRenameMissingSource()
    {
        $handle = new DirectoryHandle($this->fs, 'foo://foo');
        $foo = Mockery::mock('VirtualfsNode\NodeContainerInterface');

        $logger = Mockery::mock('Psr\Log\LoggerInterface');
        $logger->shouldReceive('warning')->once()->with(Mockery::type('string'), [
            'origin' => 'foo://foo',
            'target' => 'foo://bar',
        ]);

        $this->fs->shouldReceive('get')->once()->with('/foo');
        $this->fs->shouldReceive('get')->times(2)->with(DIRECTORY_SEPARATOR);
        $this->fs->shouldReceive('getLogger')->once()->withNoArgs()->andReturn($logger);

        $handle->rename('foo://bar');
    }

}
