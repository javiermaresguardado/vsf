<?php

namespace Virtualfs\Exception;

use Exception;
use RuntimeException;
use Virtualfs\Stream\HandleInterface;

class UnopenedHandleException extends RuntimeException implements ExceptionInterface
{
    protected $handle;
    protected $url;

    /**
     * @param HandleInterface $handle
     * @param string          $url
     * @param integer         $code
     * @param Exception       $previous
     */
    public function __construct(HandleInterface $handle, $url, $code = 0, Exception $previous = null)
    {
        $this->handle = $handle;
        $this->url = $url;

        $message = sprintf('Handle at url "%s" hasn\'t been opened.', $url);

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return HandleInterface
     */
    public function getHandle()
    {
        return $this->handle;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
