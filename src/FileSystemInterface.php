<?php

namespace Virtualfs;

use Psr\Log\LoggerInterface;
use Virtualfs\Exception\RegisteredSchemeException;
use Virtualfs\Exception\UnregisteredSchemeException;
use Virtualfs\Node\Factory\NodeFactoryInterface;
use Virtualfs\Node\Walker\NodeWalkerInterface;
use Virtualfs\Node\NodeInterface;

interface FileSystemInterface
{
    const SCHEME = 'vfs';

    /**
     * @param $path
     * @return NodeInterface
     */
    public function get($path);

    /**
     * @return LoggerInterface
     */
    public function getLogger();

    /**
     * @return NodeFactoryInterface
     */
    public function getNodeFactory();

    /**
     * @return NodeWalkerInterface
     */
    public function getNodeWalker();

    /**
     * @return string
     */
    public function getScheme();

    /**
     * @return boolean
     * @throws RegisteredSchemeException If a mounted file system exists at scheme
     */
    public function mount();

    /**
     * @return boolean
     * @throws UnregisteredSchemeException If a mounted file system doesn't exist at scheme
     */
    public function unmount();
}
