<?php

namespace Virtualfs\Node;

interface FileInterface extends NodeInterface
{
    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     */
    public function setContent($content);
}
