<?php

namespace Virtualfs\Node;

/**
 * Interface LinkInterface
 * @package VirtualfsNode
 */
interface LinkInterface extends NodeInterface
{
    /**
     * @return NodeInterface
     */
    public function getTarget();
}
