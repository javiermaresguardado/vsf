<?php

namespace Virtualfs\Node;

use DateTime;

interface NodeInterface extends StatInterface
{
    /**
     * @return DateTime
     */
    public function getDateAccessed();

    /**
     * @param DateTime $dateTime
     */
    public function setDateAccessed(DateTime $dateTime);

    /**
     * @return DateTime
     */
    public function getDateCreated();

    /**
     * @return DateTime
     */
    public function getDateModified();

    /**
     * @param DateTime $dateTime
     */
    public function setDateModified(DateTime $dateTime);

    /**
     * @return integer
     */
    public function getSize();
}
