<?php

namespace Virtualfs\Node\Factory;

use Virtualfs\Node\FileInterface;
use Virtualfs\Node\LinkInterface;
use Virtualfs\Node\NodeContainerInterface;
use Virtualfs\Node\NodeInterface;

interface NodeFactoryInterface
{
    /**
     * @param  NodeInterface[]        $children
     * @return NodeContainerInterface
     */
    public function buildDirectory(array $children = []);

   /**
    * @param  NodeContainerInterface $target
    * @return LinkInterface
    */
   public function buildDirectoryLink(NodeContainerInterface $target);

    /**
     * @param  string        $content
     * @return NodeInterface
     */
    public function buildFile($content = '');

    /**
     * @param FileInterface $target
     * @return LinkInterface
     */
    public function buildFileLink(FileInterface $target);

    /**
     * @param  array                  $tree
     * @return NodeContainerInterface
     */
    public function buildTree(array $tree);
}
