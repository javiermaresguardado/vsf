<?php

namespace Virtualfs\Node\Factory;

use LogicException;
use Virtualfs\Node\Directory;
use Virtualfs\Node\DirectoryLink;
use Virtualfs\Node\File;
use Virtualfs\Node\FileLink;
use Virtualfs\Node\FileInterface;
use Virtualfs\Node\NodeContainerInterface;

class NodeFactory implements NodeFactoryInterface
{
    public function buildDirectory(array $children = [])
    {
        return new Directory($children);
    }

    public function buildDirectoryLink(NodeContainerInterface $target)
    {
        return new DirectoryLink($target);
    }

    public function buildFile($content = '')
    {
        return new File($content);
    }

    public function buildFileLink(FileInterface $target)
    {
        return new FileLink($target);
    }

    public function buildTree(array $tree)
    {
        $nodes = [];

        foreach ($tree as $name => $content) {
            if (is_array($content)) {
                $nodes[$name] = $this->buildTree($content);
            } else {
                $nodes[$name] = $this->buildFile($content);
            }
        }

        return $this->buildDirectory($nodes);
    }
}
