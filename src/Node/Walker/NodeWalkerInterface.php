<?php

namespace Virtualfs\Node\Walker;

use Virtualfs\Node\NodeInterface;

interface NodeWalkerInterface
{
    /**
     * @param  NodeInterface $root
     * @param  string        $path
     * @return NodeInterface
     */
    public function findNode(NodeInterface $root, $path);

    /**
     * @param  NodeInterface $root
     * @param  string        $path
     * @param  callable      $fn
     * @return NodeInterface
     */
    public function walkPath(NodeInterface $root, $path, callable $fn);
}
